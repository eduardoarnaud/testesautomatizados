package br.jus.tjpb.testes.fta.estudos.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PageObject {
	
	private WebDriver driver;

	public PageObject(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	//TODOS OS ELEMENTOS DA TELA
	
	//MÉTODOS PARA MANIPULAR OS ELEMENTOS DA TELA
	public void preencherCampo(WebElement e, String valor){
		e.clear();
		e.sendKeys(valor);
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void clicarNoBotao(WebElement e){
		e.click();
	}
	
	public void selecionarValor(String idElemento, String textoVisivel){
		new Select(driver.findElement(By.id(idElemento))).selectByVisibleText(textoVisivel);
	}
	
	public WebElement getElemento(String idElemento){
		return driver.findElement(By.id(idElemento));
	}


}
