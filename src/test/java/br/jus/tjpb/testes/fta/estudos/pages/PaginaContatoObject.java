package br.jus.tjpb.testes.fta.estudos.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import br.jus.tjpb.testes.fta.estudos.pageobject.PageObject;

public class PaginaContatoObject extends PageObject{
	
	
	//TODOS OS ELEMENTOS DA PÁGINA
	@FindBy(id="nome")
	private WebElement campoNome;
	
	@FindBy(id="email")
	private WebElement campoEmail;
	
	@FindBy(id="tipomsg")
	private WebElement campoTipoMsg;
	
	@FindBy(id="idade")
	private WebElement CampoIdade;
	
	@FindBy(id="mensagem")
	private WebElement CampoMensagem;
	
	@FindBy(id="contato.button.enviarMensagem")
	private WebElement botaoEnviarSuaMensagem;
	

	public PaginaContatoObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	//MÉTODOS PARA MANIPULAR OS ELEMENTOS DA TELA
	
	//preencher campos
	//clicar em botões
	//submit
	
	
	

}
