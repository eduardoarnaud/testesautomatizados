package br.jus.tjpb.testes.fta.estudos;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import br.jus.tjpb.testes.fta.estudos.categories.AllSystem;

@RunWith(Categories.class)
@IncludeCategory({AllSystem.class})
@SuiteClasses({UC000_EnviarMensagem.class})
public class AllTests {

}
