package br.jus.tjpb.testes.fta.estudos;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import br.jus.tjpb.testes.fta.estudos.categories.AllSystem;
import br.jus.tjpb.testes.fta.estudos.categories.ProfileUser;
import br.jus.tjpb.testes.fta.estudos.pageobject.PageObject;
import br.jus.tjpb.testes.fta.estudos.pages.PaginaContatoObject;
import br.jus.tjpb.testes.fta.estudos.pages.PaginaIncialObject;
import br.jus.tjpb.testes.fta.estudos.utils.DadosUtils;

@Category({AllSystem.class,ProfileUser.class})
public class UC000_EnviarMensagem {

	private WebDriver driver;
	
	//PAGEOBJECT GENERICO
	//private PageObject po;
	
	//PAGE OBJECT ESPECIFICO
	private PaginaIncialObject paginaIncial;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//PAGEOBJECT GENERICO
		//po = new PageObject(driver);
		
		//PAGE OBJECT ESPECIFICO - PaginaIncial
		paginaIncial = new PaginaIncialObject(driver);
		PageFactory.initElements(driver, PaginaIncialObject.class); //Necessário por causa da notação @FindBy
		
	}

	@Test
	public void testEnviarMensagemFluxoBasico() throws Exception {
		driver.get(DadosUtils.getString("baseUrl"));
		
		//FORMA 01
		//driver.findElement(By.id("welcome.button.contato")).click();
		//FORMA 02
		//po.clicarNoBotao(po.getElemento("welcome.button.contato"));
		//FORMA 03
		PaginaContatoObject paginaContato = paginaIncial.acessarPaginaContato();
		
		//FORMA 01
		//driver.findElement(By.id("nome")).clear();
		//driver.findElement(By.id("nome")).sendKeys("Carlos Diego");
		//FORMA 02
		//po.preencherCampo(po.getElemento("nome"), "Carlos Diego");
		
		
		//driver.findElement(By.id("email")).clear();
		//driver.findElement(By.id("email")).sendKeys("diegoquirino@gmail.com");
		po.preencherCampo(po.getElemento("email"), "diegoquirino@gmail.com");
		
		//driver.findElement(By.id("mensagem")).clear();
		//driver.findElement(By.id("mensagem")).sendKeys("Mensagem de dúvida");
		po.preencherCampo(po.getElemento("mensagem"), "Mensagem de dúvida");
		
		//driver.findElement(By.id("contato.button.enviarMensagem")).click();
		po.clicarNoBotao(po.getElemento("contato.button.enviarMensagem"));
		
	    assertEquals("duvida-Menor que 18 anos: Mensagem enviada com sucesso!", driver.findElement(By.cssSelector("strong")).getText());
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
