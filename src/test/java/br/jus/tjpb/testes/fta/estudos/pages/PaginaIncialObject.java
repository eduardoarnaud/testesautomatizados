package br.jus.tjpb.testes.fta.estudos.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import br.jus.tjpb.testes.fta.estudos.pageobject.PageObject;

public class PaginaIncialObject extends PageObject{
	
	@FindBy(id="welcome.button.contato")
	private WebElement botaoContato;

	public PaginaIncialObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public PaginaContatoObject acessarPaginaContato(){
		super.clicarNoBotao(botaoContato);
		PaginaContatoObject contato = new PaginaContatoObject(getDriver());
		PageFactory.initElements(getDriver(), contato);
		
		return contato;
	}

}
