package br.jus.tjpb.testes.fta.estudos.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class DadosUtils {
	private static final String BUNDLE_NAME = "br.jus.tjpb.testes.fta.estudos.utils.dados"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private DadosUtils() {
		super();
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
